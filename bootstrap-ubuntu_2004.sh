#!/bin/bash

sudo apt update

sudo mkdir -p $HOME/.local/bin
sudo mkdir $HOME/projects

sudo chown -R $USER: $HOME

sudo apt install software-properties-common
sudo add-apt-repository -y ppa:deadsnakes/ppa
sudo apt install -y python3.8

sudo apt install -y fzf jq bat silversearcher-ag i3 htop stow zsh fonts-firacode rustc curl

sudo chsh -s /usr/bin/zsh

curl -L https://sw.kovidgoyal.net/kitty/installer.sh | sh /dev/stdin launch=n
sudo ln -s $HOME/.local/kitty.app/bin/kitty /usr/local/bin
ln -s $HOME/.local/kitty.app/bin/kitty $HOME/.local/bin/
sed -i "s|Icon=kitty|Icon=$HOME/.local/kitty.app/share/icons/hicolor/256x256/apps/kitty.png|g" $HOME/.local/share/applications/kitty.desktop

wget https://github.com/starship/starship/releases/latest/download/starship-x86_64-unknown-linux-musl.tar.gz
tar xvzf starship-x86_64-unknown-linux-musl.tar.gz
sudo mv starship /usr/local/bin
rm -rf starship-x86_64-unknown-linux-musl.tar.gz

sudo apt install -y apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
sudo apt update
sudo apt install -y docker-ce

cargo install exa

curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com focal main"
sudo apt-get update && sudo apt-get install -y terraform
# wget https://github.com/juliosueiras/terraform-lsp/releases/download/v0.0.12/terraform-lsp_0.0.12_linux_amd64.tar.gz

curl -LO https://github.com/neovim/neovim/releases/download/v0.5.0/nvim.appimage
chmod u+x nvim.appimage
sudo mv nvim.appimage $HOME/.local/bin/nvim

sudo apt install -y python3-pip
pip3 install pynvim
pip3 install 'python-lsp-server[all]'

stow kitty
mv $HOME/.zshrc $HOME/.zshrc.default
stow zsh
stow nvim
stow git
stow i3
stow ag

export PATH="$HOME/.cargo/bin:$HOME/.cargo/bin:$HOME/.local/kitty.app/bin:$HOME/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin:$HOME/.fzf/bin:$HOME/.direnv/"

source $HOME/.zshrc
