#!/bin/bash

sudo mkdir $HOME/projects

export PATH="$HOME/.cargo/bin:$HOME/.cargo/bin:$HOME/.local/kitty.app/bin:$HOME/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin:$HOME/.fzf/bin:$HOME/.direnv/"

sudo pacman -S --noconfirm docker python go podman skopeo fzf jq the_silver_searcher i3 exa terraform neovim zsh htop stow curl python-pip
sudo pacman -S --noconfirm rust kitty starship
sudo pip install pynvim 'python-lsp-server[all]'

nvim --headless +PackerInstall +qa

mv $HOME/.zshrc $HOME/.zshrc.default
stow zsh

sudo chsh -s /usr/bin/zsh

stow kitty
stow nvim
stow git
stow i3
stow ag
